class StaticPagesController < ApplicationController
  def home
    # 何も書かないとデフォルトの動き
    # views/static_pages/home.html.erbを表示する
    # レイアウトはviews/laouts/application.html.erbを見るのがデフォルト
    @micropost = current_user.microposts.build if logged_in?
  end

  def help
  end
  
  def about
  end

  def contact
  end
  
end
