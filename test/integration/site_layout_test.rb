require 'test_helper'

# 今回の目的は、アプリケーションのHTML構造を調べて、レイアウトの各リンクが正しく動くかどうかチェックすることです。つまり、
# ルートURL (Homeページ) にGETリクエストを送る
# 正しいページテンプレートが描画されているかどうか確かめる
# Home、Help、About、Contactの各ページへのリンクが正しく動くか確かめる

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "layout links" do
    get root_path
    # ?には自動的にroot_pathなどが入る
    assert_template 'static_pages/home'
    # homeページに対するリンク
    # assert_select "a[href=?]", root_path, count: 4
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end
end
